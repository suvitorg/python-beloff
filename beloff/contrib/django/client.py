# -*- coding: utf-8 -*-
from django.conf import settings
import slumber
from beloff.client import Client, conf


class DjangoClient(Client):
    def __init__(self):
        self.username = settings.BELOFF_USERNAME
        self.api_key = settings.BELOFF_API_KEY
        self.list_id = settings.BELOFF_LIST_ID
        client_host = getattr(settings,
                              'BELOFF_CLIENT_HOST',
                              conf.CLIENT_HOST)
        self.api = slumber.API(client_host)
