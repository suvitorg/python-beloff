# -*- coding: utf-8 -
#
# This file is part of python-beloff released under the MIT license. 
# See the NOTICE for more information.

import os
import sys
from setuptools import setup, find_packages

from beloff import VERSION


setup(
    name='python-beloff',
    version=VERSION,
    description='python client for http://chernoff-list.ru service',
    long_description=file(
        os.path.join(
            os.path.dirname(__file__),
            'README.md'
        )
    ).read(),
    author='Suvit Org',
    author_email='mail@suvit.ru',
    license='MIT',
    url='http://bitbucket.org/suvitorg/python-beloff',
    zip_safe=False,
    packages=find_packages(exclude=['docs', 'examples', 'tests']),
    install_requires=file(
        os.path.join(
            os.path.dirname(__file__),
            'requirements.txt'
        )
    ).read().split(),
    include_package_data=True,
)
