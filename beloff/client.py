# -*- coding: utf-8 -*-
import slumber
from slumber.exceptions import HttpClientError
import conf


class Client(object):
    def __init__(self, list_id, username=None, api_key=None):
        self.username = username
        self.api_key = api_key
        self.api = slumber.API(conf.CLIENT_HOST)
        self.list_id = list_id

    def get_list(self):
        if not hasattr(self, 'list'):
            if isinstance(self.list_id, int) or self.list_id.isdigit():
                self.list = self.api.list(self.list_id).get(username=self.username,
                                                            api_key=self.api_key)
            else:
                lists = self.api.list.get(list_slug=self.list_id,
                                          username=self.username,
                                          api_key=self.api_key)
                self.list = lists['objects'] and lists['objects'][0] or None

        if not self.list:
            raise HttpClientError(u'Список "%s" пользователя "%s" не найден' %\
                                  (self.list_id, self.username)
                                  )
        return self.list

    def add_to_list(self, **kwargs):
        data=dict(list=self.get_list(),
                  **kwargs)
        params = dict(data=data)
        if self.username:
            params['username'] = self.username
        if self.api_key:
            params['api_key'] = self.api_key

        bucket = self.api.bucket.post(**params)

        return True

    def check_entry(self, value, field=None):
        params = dict(value=value,
                      list=self.get_list()['id'])
        if field:
            params['field'] = field
        if self.username:
            params['username'] = self.username
        if self.api_key:
            params['api_key'] = self.api_key

        result = self.api.entry.get(**params)

        return bool(result['meta']['total_count'])

    def view_list(self, list_id=None):
        list_id = list_id or self.get_list()['id']
        params = dict(list=list_id)
        if self.username:
            params['username'] = self.username
        if self.api_key:
            params['api_key'] = self.api_key

        return self.api.bucket.get(**params)
