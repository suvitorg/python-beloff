from requests.exceptions import RequestException
from slumber.exceptions import SlumberBaseException

common_errors = (RequestException,
                 SlumberBaseException,
                 )